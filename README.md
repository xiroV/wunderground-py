# NOTICE
This repository will most likely not receive any further updates. I still use it for my own purposes,
but everything is hard-coded which makes it quite unusable/unflexible for much other purposes than displaying
weather information in Conky or similar. If I ever get the time I'll try to simplify the overall structure, but
feel free to use it as you please.

# Introduction
This is a weather plugin written in Python using the Wunderground API. The primary goal was to make a simple weather plugin which could be used with Conky + i3bar (Conky as an replacement to i3status), however since it's basicly just a Python script pulling weather information, you can use it however you like.

The plugin will pull weather information from [Weather Underground](https://www.wunderground.com/?apiref=7e6536cf90e4891d) every 30 minutes (default), and you will need your own API key from [here](https://www.wunderground.com/weather/api/).

The plugin is written using Python 3.5, and hence is also only tested with Python 3.5.

# Dependencies
- Python 3.5

  should work with any Python3.x though
- [Optional] [Weather Icons](https://erikflowers.github.io/weather-icons/)
    
    By default the script makes use of the Weather Icons (they are really pretty!), but if you don't need them, you can simply remove them from the script.

# Install
 1. Extract the contents to a directory ```dir```
 2. Rename the ```config_sample``` file to ```config``` and modify accordingly:
   - ```api_key``` Your API key from [Wunderground.com](https://www.wunderground.com/weather/api/). 
   - ```country``` This variable depends on wether you live in the US or not:
     - If you live in the US, this should be the 2-letter state code, e.g. ```OR``` (for Oregon), ```CA``` (for California), etc.
     - If you live in any other country than the US, this should simply be the english name of the country, e.g. ```Denmark```, ```Germany```, ```Ireland```, etc.
   - ```city``` The name of the city/area you want to get your weather data on.

   If you are unsure about what you should write for ```country``` or ```city```, try searching [Weather Underground](https://www.wunderground.com/?apiref=7e6536cf90e4891d).

# For use with Conky
 - Insert ```{execp python ~/__dir__/weather.py today}``` in your ```.conkyrc``` to get weather from today.

# Parameters
```today``` Get current degrees with a matching weather icon, e.g. ``` 8```

```today_prec``` Get precipitation for today, e.g. ``` 0 mm```

```day1``` Get tomorrows 3-letter name, icon, degree interval and precipitation, e.g. ```Mon  9-12/1mm ```

```day2``` Get the day after tomorrows 3-letter name, icon, degree interval and precipitation, e.g. ```Tue  9-12/1mm ```

```day3``` Get the 3-letter name, icon, degree interval and precipitation, e.g. ```Wed  9-12/1mm ```, for day 3.

If you want another structure (e.g. use other, or no icons), simply edit the conky_weather.py file.

# Additional Information
For any questions or requests, don't hesitate to create an issue, or contact me from https://alberg.org/contact
